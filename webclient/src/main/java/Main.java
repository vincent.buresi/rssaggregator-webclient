import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;

public class Main {
    public static final String TOKEN = "token";
    public static final String LOGIN_FAILED = "login_failed";

    public static void main(String[] args) {
        port(4242); //TODO Use config file.
        staticFileLocation("/public");
        get("/", serveIndexPage);
        post("/login", serveLoginPage);
        get("/logoff", serveLogoffPage);
        get("/hello", (req, res) -> "Hello World");
    }

    private static Route serveIndexPage = (Request req, Response res) -> {
        Map<String, Object> map = new HashMap<>();
        map.put(TOKEN, req.session().attribute(TOKEN));
        map.put(LOGIN_FAILED, req.session().attribute(LOGIN_FAILED));
        map.put("api", "http://0.0.0.0:4567");
        return renderTemplate("velocity/index.vm", map);
    };

    private static Route serveLoginPage = (Request req, Response res) -> { //FIXME Stub for login system.
        if (req.session() != null && req.session().attribute(TOKEN) == null) {
            String user = req.queryParams("user");
            String pass = req.queryParams("pass");

            if (user != null && user.equals("admin")
                && pass != null && pass.equals("test"))
                req.session().attribute(TOKEN, 42);
            else
                req.session().attribute(LOGIN_FAILED, "true");
        }
        res.redirect("/");
        return null;
    };

    private static Route serveLogoffPage = (Request req, Response res) -> {
        if (req.session() != null) {
            req.session().removeAttribute(TOKEN);
            req.session().removeAttribute(LOGIN_FAILED);
        }
        res.redirect("/");
        return null;
    };

    /**
     * Renders a specific template to be used as a request response.
     * @param template Name of the template to be rendered
     * @param model Map of arguments to be passed to template. Cannot be null.
     * @return The HTML formatted String resulting from the template with given arguments.
     */
    private static String renderTemplate(String template, Map model) {
        VelocityEngine engine = new VelocityEngine();
        engine.setProperty("runtime.reference.strict", true);
        engine.setProperty("resource.loader", "class");
        engine.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine templateEngine = new VelocityTemplateEngine(engine);
        return templateEngine.render(new ModelAndView(model, template));
    }
}