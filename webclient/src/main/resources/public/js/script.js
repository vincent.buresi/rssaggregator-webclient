function getFeeds(url) {
    getJson(url + "/feedaggregators", function (res) {
        var html = "";
        for (var i in res.data) {


            html +=
                "<ul class='collection with-header'>" +
                    "<li class='collection-header'>" +
                        "<h5>" +
                            "<a onclick='getFeedDetail(\"" + url + "\", \"" + res.data[i].uuid + "\")'>" +
                                res.data[i].name +
                            "</a>" +
                        "</h5>" +
                        "<a class='scfix secondary-content' onclick='deleteFeed(\"" + url + "\", \"" + res.data[i].uuid + "\")'>" +
                            "<i class='material-icons' style='color: red'>" +
                                "delete" +
                            "</i>" +
                        "</a>" +
                    "</li>";


            for (var j in res.data[i].rssurls) {
                html += "<li class='collection-item'><div>" + res.data[i].rssurls[j] + "</div></li>";
            }
            html += "</ul>";
        }
        if (html == "") {
            document.getElementById("feeds").innerHTML = "You don't have any feed to display. Consider adding some.";
        } else
            document.getElementById("feeds").innerHTML = html;
    });
}

function addFeed(url) {
    var json = {
            "name": document.getElementById("add-feed-name-field").value,
            "rssurls": [
                document.getElementById("add-feed-url-field").value
            ]
    };
    var body = JSON.stringify(json);
    console.log(body);
    postJson(url + "/feedaggregators", body, function (res) {
        document.getElementById("add-feed-name-field").value = "";
        document.getElementById("add-feed-url-field").value = "";
        getFeeds(url);
    });
}

function deleteFeed(url, uuid) {
    console.log("deleting " + uuid);
    deleteJson(url + "/feedaggregators/" + uuid, function (res) {
        getFeeds(url);
    });
}

function getFeedDetail(url, uuid) {
    console.log("getting details" + uuid);
    getJson(url + "/feedaggregators/" + uuid, function (res) {
        var html = "";
        for (var i in res.data) {
            html += "<div class='feed-detail'><h5><a href='" + res.data[i].link + "'>" + res.data[i].title + "</a></h5>";
            html += "<div class='feed-description'>" + res.data[i].description + "</div>";
            html += "<div class='feed-author'>" + res.data[i].author + "</div>";
            html += "<div class='feed-comments'>" + res.data[i].comments + "</div>";
            html += "<div class='feed-publication-date'>" + res.data[i].pubDate + "</div>";
            html += "</div>";
        }
        if (html == "") {
            document.getElementById("feed-detail").innerHTML = "This feed seems empty.";
        } else
            document.getElementById("feed-detail").innerHTML = html;
    });
}

function showDeleteConfirm(url) {
    if (confirm("Are you sure you want to delete you current account and all of its content ? This action is irreversible.")) {
        deleteJson(url + "/users/delete", function (res) {
            logoff();
            document.getElementById("username-field").value = "";
            alert("You successfully deleted your account");
        });
    }
}

function submitPasswordChange(url) {
    var pass1 = document.getElementById("change-pass-1").value;
    var pass2 = document.getElementById("change-pass-2").value;
    if (pass1 === pass2) {
        var json = {
            "password": pass1
        };
        var body = JSON.stringify(json);
        console.log(body);
        postJson(url + "/users/changepassword", body, function (res) {
            displayChangePass(false);
            alert("Password successfully changed !");
            logoff();
        });
    } else {
        alert("Password and verification should match !");
    }
}

function postJson(url, body, callback) {
    return requestJson(url, callback, "POST", body, "Error : The request failed to complete.");
}

function getJson(url, callback) {
    return requestJson(url, callback, "GET", "", "Error : The request failed to complete.");
}

function deleteJson(url, callback) {
    return requestJson(url, callback, "DELETE", "", "Error : The request failed to complete.");
}

function requestJson(url, callback, requestType, body, errorMessage) {
    var token = sessionStorage.token;
    if (token == null) {
        alert("You need to be connected to do this action");
        return ;
    }
    var xhttp = new XMLHttpRequest();
    xhttp.open(requestType, url, true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.setRequestHeader('Authorization', token);
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log('responseText:' + xhttp.responseText);
            try {
                var res = JSON.parse(xhttp.responseText);
            } catch (err) {
                console.log(err.message + " in " + xmlhttp.responseText);
                return ;
            }
            callback(res);
        } else if (this.readyState == 4) {
            alert(errorMessage);
        }
    };
    xhttp.send(body);
}

function logoff() {
    sessionStorage.removeItem("token");
    document.getElementById("feed-detail").innerHTML = "";
    document.getElementById("feeds").innerHTML = "";
    document.getElementById("username-field").value = "";
    document.getElementById("password-field").value = "";
    var pass1 = document.getElementById("change-pass-1").value = "";
    var pass2 = document.getElementById("change-pass-2").value = "";
    displayChangePass(false);
    selectView();
}

function submitRegister(url) {
    userRequest(url, "/users", "Error : Could not register given username", function (res) {
        submitLogin(url);
    });
}

function submitLogin(url) {
    userRequest(url, "/newToken", "Error : Login failed", function (res) {
        sessionStorage.token = res.data.uuid;
        selectView();
        document.getElementById("password-field").value = "";
        getFeeds(url);
    });
}

function userRequest(url, extension, errorMsg, callback) {
    var xhttp = new XMLHttpRequest();
    var json = {
        "username": document.getElementById("username-field").value,
        "password": document.getElementById("password-field").value
    };
    var body = JSON.stringify(json);
    console.log(body);
    xhttp.open("POST", url + extension, true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log('responseText:' + xhttp.responseText);
            try {
                var res = JSON.parse(xhttp.responseText);
            } catch (err) {
                console.log(err.message + " in " + xmlhttp.responseText);
                return;
            }
            callback(res);
        } else if (this.readyState == 4) {
            alert(errorMsg);
            document.getElementById("password-field").value = "";
        }
    };
    xhttp.send(body);
}

function toggleLogin() {
    console.log("toggle login");
    document.getElementById("loginBlock").style.display = "block";
    document.getElementById("contentBlock").style.display = "none";
    document.getElementById("disconnectBlock").style.display = "none";
}

function toggleContent() {
    console.log("toggle content");
    document.getElementById("loginBlock").style.display = "none";
    document.getElementById("contentBlock").style.display = "block";
    document.getElementById("disconnectBlock").style.display = "block";
}

function displayChangePass(visible) {
    if (visible)
        document.getElementById("changePasswordBlock").style.display = "block";
    else
        document.getElementById("changePasswordBlock").style.display = "none";
}

function toggleChangePass() {
    if (document.getElementById("changePasswordBlock").style.display === "block")
        displayChangePass(false);
    else
        displayChangePass(true);
}

function selectFirstView(url) {
    if (sessionStorage.token)
        getFeeds(url);
    selectView();
}

function selectView() {
    if (sessionStorage.token)
        toggleContent();
    else
        toggleLogin();
}
